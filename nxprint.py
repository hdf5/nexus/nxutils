import numpy
import h5py

INDENT = "  "


def typeshape(value) -> str:
    if not isinstance(value, (numpy.ndarray, h5py.Dataset)):
        return type(value).__name__

    if str(value.dtype) == "object":
        dtype = "str"
    else:
        dtype = value.dtype
    shape = ",".join(map(str, value.shape))
    if shape:
        shape = f"[{shape}]"
    return f"{dtype}{shape}"


def nxprint(name: str, h5item, level: int = 0) -> None:
    is_dataset = isinstance(h5item, h5py.Dataset)
    if is_dataset:
        vt = typeshape(h5item)
        if h5item.size > 5:
            print(f"{INDENT*level}{name}: {vt}")
        else:
            v = h5item[()]
            if isinstance(v, bytes):
                v = v.decode()
            if isinstance(v, str):
                if len(v) > 20:
                    v = v[:20] + "..."
                print(f'{INDENT*level}{name}: {vt} = "{v}"')
            else:
                print(f"{INDENT*level}{name}: {vt} = {v}")

    else:
        NX_class = h5item.attrs.get("NX_class")
        if NX_class:
            try:
                NX_class = NX_class.decode()
            except AttributeError:
                pass
            print(f"{INDENT*level}{name}:{NX_class}")
        else:
            print(f"{INDENT*level}{name}")

    level += 1
    for k, v in h5item.attrs.items():
        if k == "NX_class":
            continue
        if isinstance(v, bytes):
            v = v.decode()
        vt = typeshape(v)
        if isinstance(v, str):
            if len(v) > 20:
                v = v[:20] + "..."
            print(f'{INDENT*level}@{k}: {vt} = "{v}"')
        else:
            print(f"{INDENT*level}@{k}: {vt} = {v}")

    if is_dataset:
        return

    for name, h5subitem in h5item.items():
        if h5subitem is None:
            continue
        nxprint(name, h5subitem, level=level)


if __name__ == "__main__":
    filename = "data/lambda_integrated.h5"
    # filename = "data/p45-1168.nxs"
    with h5py.File(filename, mode="r") as f:
        nxprint("/", f)
